Django==2.2
django-environ==0.4.5
psycopg2-binary==2.7.6.1
pytz==2018.7
sqlparse==0.3.0
